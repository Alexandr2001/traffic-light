package ru.vaa.trafficlight

import android.app.Activity
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun applyColor(view: View) {
        when (view.id) {
            R.id.btnRed ->
                relativeLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.btnRed))
            R.id.btnYellow ->
                relativeLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.btnYellow))
            R.id.btnGreen ->
                relativeLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.btnGreen))
        }
    }
}
